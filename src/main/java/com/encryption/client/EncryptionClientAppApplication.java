package com.encryption.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EncryptionClientAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncryptionClientAppApplication.class, args);
	}

}
