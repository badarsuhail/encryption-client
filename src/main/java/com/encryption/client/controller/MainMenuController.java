package com.encryption.client.controller;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.encryption.client.services.ClientService;
import com.encryption.client.services.SecretService;

@Component
public class MainMenuController implements CommandLineRunner {

	private ClientService clientService;
	private SecretService secretService;
	private Scanner scanner;
	private String userName;

	@Autowired
	public MainMenuController(ClientService clientService, SecretService secretService) {
		this.clientService = clientService;
		this.secretService = secretService;
		this.scanner = new Scanner(System.in);
	}

	@Override
	public void run(String... args) {
		int menuOption = 0;

		while (menuOption != 5) {
			showMenu();
			menuOption = scanner.nextInt();
			scanner.nextLine();

			switch (menuOption) {
			case 1:
				register();
				break;
			case 2:
				getServerKey();
				break;
			case 3:
				storeSecret();
				break;
			case 4:
				register();
				break;
			default:
				break;
			}
		}
		scanner.close();
		System.out.println("Exiting the Client");
	}

	private void register() {
		System.out.println("\n Please enter UserName: ");
		this.userName = scanner.nextLine();
		String response = this.clientService.register(this.userName);

		System.out.println("Response Received: " + response);
		System.out.println();
	}

	private void getServerKey() {
		System.out.println("\n GETTING SERVER KEY \n\n ");
		String response = this.secretService.getServerKey();
		System.out.println(response);
		System.out.println();
	}
	
	private void storeSecret() {
		System.out.println("\n Please enter the Sceret Name: ");
		String secretName = scanner.nextLine();
		System.out.println("\n Please enter the Sceret Message: ");
		String secretMessage = scanner.nextLine();
		String response = this.secretService.storeSecret(userName, secretName, secretMessage);

		System.out.println("Response Received: " + response);
		System.out.println();
		
	}

	private static void showMenu() {
		System.out.println("=========== Select one of the following ==========");
		System.out.println("1- Register User");
		System.out.println("2- Get Server Key");
		System.out.println("3- Store Secret");
		System.out.println("4- Get Secret");
		System.out.println("5- Exit");
		System.out.print("\n:");
	}
}
