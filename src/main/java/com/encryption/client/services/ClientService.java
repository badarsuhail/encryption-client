package com.encryption.client.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.encryption.client.util.EncryptionUtil;
import com.encryption.client.util.WebClient;

@Service
public class ClientService {

	private WebClient webClient;
	private EncryptionUtil encryptionUtil;

	@Autowired
	public ClientService(WebClient webClient, EncryptionUtil encryptionUtil) {
		this.webClient = webClient;
		this.encryptionUtil = encryptionUtil;
	}

	public String register(String userName) {
		try {
			Map<String, String> registerParams = new HashMap<String, String>();
			registerParams.put("userName", userName);
			registerParams.put("publicKey", this.encryptionUtil.getPublicKey().toString());
			return webClient.post("client/register", registerParams);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
