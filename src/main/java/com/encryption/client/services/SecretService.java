package com.encryption.client.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.encryption.client.util.EncryptionUtil;
import com.encryption.client.util.WebClient;

@Service
public class SecretService {

	private WebClient webClient;
	private EncryptionUtil encryptionUtil;

	@Autowired
	public SecretService(WebClient webClient, EncryptionUtil encryptionUtil) {
		this.webClient = webClient;
		this.encryptionUtil = encryptionUtil;
	}

	public String getServerKey() {
		try {
			String serverPublicKey = webClient.get("secret/getServerKey", "");
			this.encryptionUtil.setServerPublicKey(serverPublicKey);
			return serverPublicKey;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public String storeSecret(String userName, String secretName, String secretMsg) {
		try {
			String encryptedSecret = encryptionUtil.encryptText(secretMsg, encryptionUtil.getServerPublicKey());
			String hashPayload = userName+","+secretName;
			String signature =  encryptionUtil.encryptText(hashPayload, encryptionUtil.getPrivateKey());
			
			Map<String, String> params = new HashMap<String, String>();
			params.put("userName", userName);
			params.put("secretName", secretName);
			params.put("encryptedSecret", encryptedSecret);
			params.put("signature", signature);

			return webClient.post("secret/storeSecret", params);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
