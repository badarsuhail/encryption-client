package com.encryption.client.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
//import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

@Component
public class EncryptionUtil {

	private static final String PUBLIC_KEY_PATH = "KeyPair/publicKey";
	private static final String PRIVATE_KEY_PATH = "KeyPair/privateKey";
	private static final String SERVER_PUBLIC_KEY_PATH = "KeyPair/serverPublicKey";
	private Cipher cipher;

	public EncryptionUtil() {
		KeyPairGenerator keyGen;
		try {
			keyGen = KeyPairGenerator.getInstance("RSA");
			this.cipher = Cipher.getInstance("RSA");
			keyGen.initialize(2048);
			createKeys(keyGen);
		} catch (NoSuchAlgorithmException | IOException | NoSuchPaddingException ex) {
			throw new RuntimeException(ex);
		}
	}

	private static void createKeys(KeyPairGenerator keyGen) throws IOException {
		KeyPair pair = keyGen.generateKeyPair();
		String pemPrivateKey = "-----BEGIN PRIVATE KEY-----\r\n"
				+ java.util.Base64.getMimeEncoder().encodeToString(pair.getPrivate().getEncoded())
				+ "\r\n-----END PRIVATE KEY-----";

		System.out.println("-----BEGIN PUBLIC KEY-----");
		System.out.println(java.util.Base64.getMimeEncoder().encodeToString(pair.getPublic().getEncoded()));
		System.out.println("-----END PUBLIC KEY-----");

		writeToFile(PUBLIC_KEY_PATH, pair.getPublic().getEncoded());
		writeToFile(PRIVATE_KEY_PATH, pair.getPrivate().getEncoded());
	}

	public void setServerPublicKey(String serverPublicKey) throws Exception {
		writeToFile(SERVER_PUBLIC_KEY_PATH, serverPublicKey.getBytes("UTF-8"));
	}

	public PublicKey getServerPublicKey() throws Exception {
		byte[] keyBytes = Files.readAllBytes(new File(SERVER_PUBLIC_KEY_PATH).toPath());
		return getPublicKeyFromString(new String(keyBytes));

//		return new String(readFromFile(SERVER_PUBLIC_KEY_PATH));
	}

	public static PublicKey getPublicKeyFromString(String key) throws IOException, GeneralSecurityException {
		String publicKeyPEM = key;
		publicKeyPEM = publicKeyPEM.replace("-----BEGIN PUBLIC KEY-----\r\n", "");
		publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");
		byte[] encoded = Base64.decodeBase64(publicKeyPEM);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey pubKey = kf.generatePublic(new X509EncodedKeySpec(encoded));
		return pubKey;
	}

	// https://docs.oracle.com/javase/8/docs/api/java/security/spec/PKCS8EncodedKeySpec.html
	public PrivateKey getPrivateKey() throws Exception {
		byte[] keyBytes = Files.readAllBytes(new File(PRIVATE_KEY_PATH).toPath());
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	// https://docs.oracle.com/javase/8/docs/api/java/security/spec/X509EncodedKeySpec.html
	public PublicKey getPublicKey() throws Exception {
		byte[] keyBytes = Files.readAllBytes(new File(PUBLIC_KEY_PATH).toPath());
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	public String encryptText(String msg, Key key) throws NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
		return Base64.encodeBase64String(cipher.doFinal(msg.getBytes("UTF-8")));
	}

	public String decryptText(String msg, Key key)
			throws InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		return new String(cipher.doFinal(Base64.decodeBase64(msg)), "UTF-8");
	}

	private static void writeToFile(String path, byte[] key) throws IOException {
		File f = new File(path);
		f.getParentFile().mkdirs();

		FileOutputStream fos = new FileOutputStream(f);
		fos.write(key);
		fos.flush();
		fos.close();
	}

}
